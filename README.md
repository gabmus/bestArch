# System

## Make an encrypted install

**NOTE**: In this tutorial I will assume you're working with a single drive indicated as `${DISK}`. I will refer to `${DISK}1`, `${DISK}2` etc as partition 1, partition 2 and so on. If you want to copy paste these commands, you can if you set the `DISK` variable in your shell to something like `/dev/sda`. If you have an nvme drive you CANNOT do this, since an nvme drive is indicated as `/dev/nvme0n1` while partitions are indicated as `/dev/nvme0n1p1`, `/dev/nvme0n1p2` etc. I will also assume you want to install using UEFI/systemd-boot.

**Raw partition table overview**

| Partition  | Format | Mount point             |
| ---------- | ------ | ----------------------- |
| `${DISK}1` | FAT32  | `/boot`                 |
| `${DISK}2` | LUKS   | *No direct mount point* |

Create a **GPT** partition table with `parted` as follows:

```bash
parted ${DISK}
mklabel gpt
q
```

Use `gdisk` to create the partitions. **NOTE**: I will write `# press enter` when I need you to just press the *enter* key without writing anything

```bash
gdisk ${DISK}
n
# press enter
# press enter
+400M
ef00
n
# press enter
# press enter
# press enter
8e00
w
y
```

Complete the setup as follows

```bash
# Format the first partition to *FAT32*.
mkfs.vfat -F32 ${DISK}1
# Format the second partition as a LUKS encrypted container
cryptsetup luksFormat --type luks2 ${DISK}2
# Open the just created container
cryptsetup open ${DISK}2 cryptlvm
# Create a physical volume on top of the opened LUKS container
pvcreate /dev/mapper/cryptlvm
# Create a volume group with a name you want (I usually choose MasterVolumeGroup)
# and add the previously created physical volume to it
vgcreate MasterVolumeGroup /dev/mapper/cryptlvm
# Create the logical volumes on the volume group for the root and home partitions
lvcreate -L 40G MasterVolumeGroup -n root
lvcreate -l 100%FREE MasterVolumeGroup -n home
# Format the newly created logical volumes
mkfs.ext4 /dev/MasterVolumeGroup/root
mkfs.ext4 /dev/MasterVolumeGroup/home
# Mount the filesystem
mount /dev/MasterVolumeGroup/root /mnt
mkdir /mnt/home
mkdir /mnt/boot
mount /dev/MasterVolumeGroup/home /mnt/home
mount ${DISK}1 /mnt/boot
```

Before proceeding with the installation, you have to edit `/etc/mkinitcpio.conf` as follows:

- Add **`keyboard keymap`** after `base udev autodetect` in the **`HOOKS`** array
- Add **`encrypt lvm2`** after `consolefont modconf block` in the **`HOOKS`** array

Proceed with the remaining part of the installation as normal. Refer to [Use systemd-boot](#use-systemd-boot) to install the systemd-boot bootloader.

## Use systemd-boot

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Systemd-boot#Installation)

Requires you to be able to boot in UEFI mode (not MBR).

You need to have a `/boot` partition formatted in FAT32 (usually I make it 400 MBs, even if it's a little too much).

Assuming you have all your file systems mounted to their proper locations AND that you are already chroot-ed in your installed system.

```bash
sudo bootctl --path=/boot install
```

Create `/boot/loader/entries/arch.conf` like follows:

```
title		Arch Linux
linux		/vmlinuz-linux
# uncomment one of the two lines below to install the intel or amd microcode
# initrd		/intel-ucode.img
# initrd		/amd-ucode.img
initrd		/initramfs-linux.img
# uncomment `nvidia-drm.modeset=1` if you want to enable nvidia drm kernel mode setting
options		root=UUID=ROOT_PARTITION_UUID rw quiet # nvidia-drm.modeset=1
# use the following options row in alternative to this ^^^ one if you have an encrypted setup
# options cryptdevice=UUID=ENCRYPTED_PARTITION_UUID:cryptlvm root=/dev/MasterVolumeGroup/root rw quiet
```

Where `ROOT_PARTITION_UUID` or `ENCRYPTED_PARTITION_UUID` can be obtained from the command `lsblk -f` (use the UUID of the partition mounted as `/`).

You may want to edit `/boot/loader/loader.conf` to set a timeout (format: `timeout TIME_IN_SECONDS`) and to add a line that says `default arch-*`.

Run:

```bash
sudo systemctl enable systemd-boot-update.service
```

This will update systemd-boot on the next boot.

## Microcode updates

[Arch wiki reference](https://wiki.archlinux.org/index.php/Microcode#Enabling_Intel_microcode_updates)

### AMD

```bash
sudo pacman -S amd-ucode
```

Edit `/boot/loader/entries/arch.conf` so that the first `initrd` line is the following:

```
initrd        /amd-ucode.img
```

### Intel

```bash
sudo pacman -S intel-ucode
```

Edit `/boot/loader/entries/arch.conf` so that the first `initrd` line is the following:

```
initrd        /intel-ucode.img
```

## Compress initramfs with lz4

Make sure `lz4` is installed.

Edit `/etc/mkinitcpio.conf`:

- Add `lz4 lz4_compress` to the `MODULES` list (delimited by `()`)
- Uncomment or add the line saying `COMPRESSION="lz4"`
- Add a line saying `COMPRESSION_OPTIONS="-9"`

Run `sudo mkinitcpio -p linux` to apply the mkinitcpio.conf changes.

## Limit journald log size

Edit `/etc/systemd/journald.conf`:

- Uncomment `SystemMaxUse=` and append `200M` (or any size you like).

## Disable core dumps

To improve performance and save disk space.

Edit `/etc/systemd/coredump.conf`, under `[Coredump]` uncomment `Storage=external` and replace it with `Storage=none`. Then run `sudo systemctl daemon-reload`. This alone disables the saving of coredumps but they are still in memory.

If you want to disable core dumps completely add `* hard core 0` to `/etc/security/limits.conf`.

## Enable deep sleep suspension mode

Verify that you're using the inefficient `s2idle` sleep state before continuing:

```bash
cat /sys/power/mem_sleep
```

| Inefficient     | Efficient       |
|-----------------|-----------------|
| `[s2idle] deep` | `s2idle [deep]` |

Add `mem_sleep_default=deep` to the kernel command line arguments.

## Change IO Scheduler

## Change CPU governor

[Arch Wiki reference](https://wiki.archlinux.org/index.php/CPU_frequency_scaling)

```bash
sudo pacman -S cpupower
```

To change the governor for the current session run `sudo cpupower frequency-set -g performance`.

To change the governor on boot create a systemd service.

Create `/etc/systemd/system/cpupower.service`:

```
[Unit]
Description=Set CPU governor to performance

[Service]
Type=oneshot
ExecStart=/usr/bin/cpupower -c all frequency-set -g performance

[Install]
WantedBy=multi-user.target
```

Finally run `sudo systemctl enable cpupower.service`.

*NB: the default governor is powersave and you may want to leave it as it is.*

Create `/etc/udev/rules.d/50-scaling-governor.rules` as follows:

```
SUBSYSTEM=="module", ACTION=="add", KERNEL=="acpi_cpufreq", RUN+=" /bin/sh -c ' echo performance > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor ' "
```

## Setting up Plymouth

*NOTE: this setup implies that you use paru (AUR helper), gdm (display manager), and the default arch kernel.*

```bash
paru -S plymouth-git gdm-plymouth
```

Edit `/etc/mkinitcpio.conf`:

- In `HOOKS` after `base udev` insert `plymouth`
- If you're using encryption, in `HOOKS` replace `encrypt` with `plymouth-encrypt`
- In `MODULES` insert your GPU driver module name as first item
  - For Intel GPUs: `i915`
  - For AMD GPUs: `radeon` *(note: this is untested)*
  - For NVIDIA GPUs: `nvidia` *(note: this is untested)* 
  - For KVM/qemu VMs: `qxl`

Edit `/boot/loader/entries/arch-linux.conf`: add these arguments in the kernel options (append to the `options` section): `quiet splash loglevel=3 rd.udev.log_priority=3 vt.global_cursor_default=1`

```bash
sudo systemctl disable gdm
sudo systemctl enable gdm-plymouth
sudo mkinitcpio -p linux
```

## Create a swap file

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Swap#Swap_file)

A form of swap is required to enable hibernation.

In this example we will allocate a 8G swap file.

```bash
sudo dd if=/dev/zero of=/home/swapfile bs=1M count=8192
sudo chmod 600 /home/swapfile
sudo mkswap /home/swapfile
sudo swapon /home/swapfile # this enables the swap file for the current session
```

Edit `/etc/fstab` adding the following line:

```
/home/swapfile none swap defaults 0 0
```

### Removing the swap file if not necessary/wanted anymore

```
sudo swapoff -a
```

Edit `/etc/fstab` and remove the swapfile entry, and finally:

```
sudo rm -f /home/swapfile
```

### Alternative route

Use systemd-swap for automated and dynamic swapfile allocation and use. Consult [the GitHub project page](https://github.com/Nefelim4ag/systemd-swap) for more info.

## Enable Hibernation

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate#Hibernation_into_swap_file)

## Enable magic sysreq

Add this line to a file inside `/etc/sysctl.d/` (ie: `99-sysctl.conf`)

```
kernel.sysrq=1
```

## Enable weekly fstrim

```bash
sudo systemctl enable fstrim.timer
```

# Package Management

## Switch to better mirrors

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Reflector)

```bash
sudo pacman -S reflector
sudo reflector --latest 200 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

## Enable colors in pacman

Edit `/etc/pacman.conf` and uncomment the row saying `Color`

## Enable parallel compilation and compression

Edit `/etc/makepkg.conf`:

- Add the following row (replace 7 with CPU threads-1): `MAKEFLAGS="-j7"`
- Edit the row saying `COMPRESSXZ=(xz -c -z -)` to `COMPRESSXZ=(xz -c -z - --threads=0)`
- `sudo pacman -S pigz` and edit the row saying `COMPRESSGZ=(gzip -c -f -n)` to `COMPRESSGZ=(pigz -c -f -n)`

## Install flatpaks in user directory with GNOME Software by default

```bash
gsettings set org.gnome.software install-bundles-system-wide false
```

## Use another computer as repository

[Arch wiki reference](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Network_shared_pacman_cache)

> If you happen to run several Arch boxes on your LAN, you can share packages so that you can greatly decrease your download times

On the source computer install `darkhttpd` and run:

```bash
sudo ln -s /var/lib/pacman/sync/*.db /var/cache/pacman/pkg
sudo -u http darkhttpd /var/cache/pacman/pkg --no-server-id
```

On the destination computer edit `/var/pacman.d/mirrorlist` and insert `Server = http://<source_machine_ip>:8080` as the first line of the file, replacing `<source_machine_ip>` with the ip of your source computer (to get a computer's ip run `ip -c a`).

You're all set up. Once you're done, close darkhttpd on the source computer and comment/delete the line you just added on the destination computer.

This could be useful if you have multiple machines on the same LAN, maybe sharing a slow connection, or even if you want to make an arch installation without an internet connection.

# Networking

## Network printing

- [Arch wiki reference (CUPS#Network)](https://wiki.archlinux.org/index.php/CUPS#Network)
- [Arch wiki reference (Avahi#Hostname_Resolution)](https://wiki.archlinux.org/index.php/Avahi#Hostname_resolution)

For good measure when using CUPS, make sure you have installed all cups packages I recommend below. They are all included in the *commonpackages* file in this repo.

```
foomatic-db foomatic-db-engine foomatic-db-gutenprint-ppds foomatic-db-nonfree foomatic-db-nonfree-ppds foomatic-db-ppds gutenprint cups cups-filters cups-pdf cups-pk-helper
```

Also make sure that your user is part of these 3 groups:

- `sys`
- `lp`
- `cups`

In one command: `sudo usermod -aG sys,lp,cups $USERNAME`.

Install `nss-mdns`.

Edit `/etc/nsswitch.conf` adding `mdns_minimal [NOTFOUND=return]` right before `resolve [!UNAVAIL=return]`.

```
hosts: ... mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] dns ...
#          ------------------------------
```

Do a `sudo systemctl start avahi-daemon.service` (or restart if necessary). Also do `sudo systemctl restart org.cups.cupsd.service` and `sudo systemctl restart cups-browsed.service` for good measure.

If you have the correct drivers for the network printer, it should now be usable. Also, if GUI printer managers fail, try the CUPS web interface at [`localhost:631`](http://localhost:631).

# Graphics

## General

### Copy monitor layout from user to GDM

GDM doesn't know how you configure your monitors. It just keep its default configuration and most of the time it's not the same of how you have them configured in your session.

To copy your user's monitors configuration over to GDM, use these commands:

```bash
sudo cp $HOME/.config/monitors.xml /var/lib/gdm/.config/
sudo chown gdm:gdm /var/lib/gdm/.config/monitors.xml
```

## Intel GPU

### Intel GPU early kernel mode setting

Edit `/etc/mkinitcpio.conf`, add the following at the end of the `MODULES` array: `intel_agp i915`

**NOTE**: on some systems (Intel+AMD GPU) adding `intel_agp` can cause issues with resume from hibernation. [Reference](https://wiki.archlinux.org/title/Kernel_mode_setting#Early_KMS_start).

## AMDGPU

### AMDGPU TearFree

Create `/etc/X11/xorg.conf.d/99-amdgpu.conf` with the following content:

```
Section "Device"
     Identifier "AMD"
     Driver "amdgpu"
     Option "TearFree" "true"
EndSection
```

### AMDGPU Variable Refresh Rate

Create or edit `/etc/X11/xorg.conf.d/99-amdgpu.conf` and make sure it has the row saying `Option "VariableRefresh" "true"`. It's also suggested that you enable TearFree along with VariableRefresh:

```
Section "Device"
     Identifier "AMD"
     Driver "amdgpu"
     Option "TearFree" "true"
     Option "VariableRefresh" "true"
EndSection
```

### AMDGPU early kernel mode setting

Edit `/etc/mkinitcpio.conf` prepending to the `MODULES` list (delimited by `()`) the following: `amdgpu`.

Run `sudo mkinitcpio -P`.

### AMDGPU enable overclocking

Add the following option to your kernel command line arguments: `amdgpu.ppfeaturemask=0xffffffff`.

If you're [using systemd-boot](#use-systemd-boot) add the line above at the end of the `options` line.

To overclock your AMD GPU I suggest using [CoreCtrl](https://gitlab.com/corectrl/corectrl).

### Override Vulkan ICD

Set the environment variable `VK_ICD_FILENAMES` to the absolute path of one of the json files in `/usr/share/vulkan/icd.d/`.

AMDGPU Pro Vulkan ICD seems to be glitchy as of now (`2019-08-27 13:41:47+02:00`). `/usr/share/vulkan/icd.d/radeon_icd.x86_64.json` seems to be working fine.

## NVIDIA

### NVIDIA DRM kernel mode setting

[Arch Wiki reference](https://wiki.archlinux.org/index.php/NVIDIA#DRM_kernel_mode_setting)

Edit `/boot/loader/entries/arch.conf` appending `nvidia-drm.modeset=1` to the `options` row.

Edit `/etc/mkinitcpio.conf` prepending to the `MODULES` list (delimited by `()`) the following: `nvidia nvidia_modeset nvidia_uvm nvidia_drm`.

Run `sudo mkinitcpio -p linux` to apply the mkinitcpio.conf changes.

Add a pacman hook to rebuild initramfs after an NVIDIA driver upgrade, create `/etc/pacman.d/hooks/nvidia.hook`:

```
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia

[Action]
Depends=mkinitcpio
When=PostTransaction
Exec=/usr/bin/mkinitcpio -P
```

*NB: Make sure the Target package set in this hook is the one you have installed (`nvidia`, `nvidia-lts` or some other different or legacy driver package name).*

Edit `/etc/gdm/custom.conf` uncommenting the row that says `WaylandEnable=false` (enabling DRM kernel mode setting usually improves performance but enables NVIDIA Wayland support for GNOME, but currently NVIDIA Wayland performance is terrible and makes for an unusable experience. While this option is not mandatory, it's highly recommended).

### Fix screen tearing with NVIDIA GPUs

First you need a base xorg config file to work with. To obtain it, run `nvidia-xconfig -o ./nvidia-xorg.conf`.

Open it up and strip out everything but the **Device** and **Screen** sections.

In the **Device** section you may want to add a line like this: `BoardName "<GPU Model>"`. For example, with my GTX 960 I have `BoardName  "GeForce GTX 960"`.

In the **Screen** section, delete everything but the lines saying **Identifier**, **Device** and **Monitor**. In the same section add the following lines:

```
Option "AllowIndirectGLXProtocol" "off"
Option "TripleBuffer" "on"
```

Finally, again in the **Screen** section, add a line that's a little bit complex, so we can build it up step by step.

You have to write a comma separated list of the video outputs that you use, corresponding resolutions, refresh rates and offsets. You can make this process easier by using xrandr.

In a terminal, type in `xrandr | grep " connected"`. You will see something like this:

```
HDMI-0 connected 1920x1080+2560+360 (normal left inverted right x axis y axis) 521mm x 293mm
DP-2 connected primary 2560x1440+0+0 (normal left inverted right x axis y axis) 597mm x 336mm
```

The initial part of the line is always `Option "metamodes" `.

Following, inside quotes, write `"<output name>: <resolution width>x<resolution height>_<refresh_rate> +<offset x>+<offset y> {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, <repeat for all outputs>"`.

For reference, here's my line: `Option "metamodes" "DP-2: 2560x1440_60 +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, HDMI-0: 1920x1080_60 +2560+360 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"`.

Again, for reference, here's my full file:

```
Section "Device"
        Identifier "Device0"
        Driver     "nvidia"
        VendorName "NVIDIA Corporation"
        BoardName  "GeForce GTX 960"
EndSection

Section "Screen"
    Identifier     "Screen0"
    Device         "Device0"
    Monitor        "Monitor0"
    Option         "AllowIndirectGLXProtocol" "off"
    Option         "TripleBuffer" "on"
    Option         "metamodes" "DP-2: 2560x1440_60 +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, HDMI-0: 1920x1080_60 +2560+360 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
EndSection
```

Once you have a file that looks like this, rename it to 20-nvidia.conf and move or copy it to `/etc/X11/xorg.conf.d/`.

### NVIDIA Optimus

![Arch Wiki reference](https://wiki.archlinux.org/title/PRIME#PRIME_render_offload)

### GNOME Wayland not available with Intel+NVIDIA GPUs

Edit `/etc/environment` and add `MUTTER_ALLOW_HYBRID_GPUS=1`

You may also need to enable [early KMS for the Intel GPU](#intel-gpu-early-kernel-mode-setting) as well as for the NVIDIA GPU by adding `nvidia-drm.modeset=1` to your kernel command line arguments.

You should also enable early kms for nvidia and add a pacman hook [as described here](https://wiki.archlinux.org/title/NVIDIA#pacman_hook).

If the above doesn't work, or if it's inconsistent, you might need to comment some lines in `/usr/lib/udev/rules.d/61-gdm.rules`. There are rules that disable Wayland altogether if certain conditions are met, and that's not always desirable.

As a reference, here's an edited version of the file, with comments saying `gabmus commented this` where I made the changes:

```udevrules
# identify virtio graphics cards to find passthrough setups
SUBSYSTEM!="virtio", GOTO="gdm_virtio_device_end"
ACTION!="add", GOTO="gdm_virtio_device_end"
ATTR{vendor}=="0x1af4", ATTR{device}=="0x0010", RUN+="/usr/bin/touch /run/udev/gdm-machine-has-virtual-gpu", ENV{GDM_MACHINE_HAS_VIRTUAL_GPU}="1", GOTO="gdm_virtio_device_end"
LABEL="gdm_virtio_device_end"

SUBSYSTEM!="pci", GOTO="gdm_pci_device_end"
ACTION!="bind", GOTO="gdm_pci_device_end"

# identify virtio graphics cards to find passthrough setups
# cirrus
ATTR{vendor}=="0x1013", ATTR{device}=="0x00b8", ATTR{subsystem_vendor}=="0x1af4", ATTR{subsystem_device}=="0x1100", RUN+="/usr/bin/touch /run/udev/gdm-machine-has-virtual-gpu", ENV{GDM_MACHINE_HAS_VIRTUAL_GPU}="1", GOTO="gdm_pci_device_end"
# vga
ATTR{vendor}=="0x1b36", ATTR{device}=="0x0100", RUN+="/usr/bin/touch /run/udev/gdm-machine-has-virtual-gpu", ENV{GDM_MACHINE_HAS_VIRTUAL_GPU}="1", GOTO="gdm_pci_device_end"
# qxl
ATTR{vendor}=="0x1234", ATTR{device}=="0x1111", RUN+="/usr/bin/touch /run/udev/gdm-machine-has-virtual-gpu", ENV{GDM_MACHINE_HAS_VIRTUAL_GPU}="1", GOTO="gdm_pci_device_end"

# disable Wayland on Hi1710 chipsets
ATTR{vendor}=="0x19e5", ATTR{device}=="0x1711", GOTO="gdm_disable_wayland"

# disable Wayland on Matrox chipsets
ATTR{vendor}=="0x102b", ATTR{device}=="0x0522", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0524", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0530", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0532", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0533", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0534", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0536", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x102b", ATTR{device}=="0x0538", GOTO="gdm_disable_wayland"

# disable Wayland on aspeed chipsets
ATTR{vendor}=="0x1a03", ATTR{device}=="0x2010", GOTO="gdm_disable_wayland"
ATTR{vendor}=="0x1a03", ATTR{device}=="0x2000", GOTO="gdm_disable_wayland"

LABEL="gdm_pci_device_end"

# disable Wayland if modesetting is disabled
KERNEL!="card[0-9]*", GOTO="gdm_nomodeset_end"
SUBSYSTEM!="drm", GOTO="gdm_nomodeset_end"
IMPORT{parent}="GDM_MACHINE_HAS_VIRTUAL_GPU"
ENV{GDM_MACHINE_HAS_VIRTUAL_GPU}!="1", RUN+="/usr/bin/touch /run/udev/gdm-machine-has-hardware-gpu"
# but keep it enabled for simple framebuffer drivers
DRIVERS=="simple-framebuffer", GOTO="gdm_nomodeset_end"
IMPORT{cmdline}="nomodeset", GOTO="gdm_disable_wayland"
LABEL="gdm_nomodeset_end"

# The vendor nvidia driver has multiple modules that need to be loaded before GDM can make an
# informed choice on which way to proceed, so force GDM to wait until NVidia's modules are
# loaded before starting up.
KERNEL!="nvidia", GOTO="gdm_nvidia_end"
SUBSYSTEM!="module", GOTO="gdm_nvidia_end"
ACTION!="add", GOTO="gdm_nvidia_end"
RUN+="/usr/bin/touch /run/udev/gdm-machine-has-vendor-nvidia-driver"

# Check if suspend/resume services necessary for working wayland support is available
########### gabmus commented this vvv
# TEST{0711}!="/usr/bin/nvidia-sleep.sh", GOTO="gdm_disable_wayland"
# TEST{0711}!="/usr/lib/systemd/system-sleep/nvidia", GOTO="gdm_disable_wayland"
# IMPORT{program}="/bin/sh -c \"sed -e 's/: /=/g' -e 's/\([^[:upper:]]\)\([[:upper:]]\)/\1_\2/g' -e 's/[[:lower:]]/\U&/g' -e 's/^/NVIDIA_/' /proc/driver/nvidia/params\""
# ENV{NVIDIA_PRESERVE_VIDEO_MEMORY_ALLOCATIONS}!="1", GOTO="gdm_disable_wayland"
# IMPORT{program}="/bin/sh -c 'echo NVIDIA_HIBERNATE=`systemctl is-enabled nvidia-hibernate`'"
# ENV{NVIDIA_HIBERNATE}!="enabled", GOTO="gdm_disable_wayland"
# IMPORT{program}="/bin/sh -c 'echo NVIDIA_RESUME=`systemctl is-enabled nvidia-resume`'"
# ENV{NVIDIA_RESUME}!="enabled", GOTO="gdm_disable_wayland"
# IMPORT{program}="/bin/sh -c 'echo NVIDIA_SUSPEND=`systemctl is-enabled nvidia-suspend`'"
# ENV{NVIDIA_SUSPEND}!="enabled", GOTO="gdm_disable_wayland"
# LABEL="gdm_nvidia_end"

# If this machine has an internal panel, take note, since it's probably a laptop
# FIXME: It could be "ghost connectors" make this pop positive for some workstations
# in the wild. If so, we may have to fallback to looking at the chassis type from
# dmi data or acpi
KERNEL!="card[0-9]-eDP-*", GOTO="gdm_laptop_check_end"
SUBSYSTEM!="drm", GOTO="gdm_laptop_check_end"
ACTION!="add", GOTO="gdm_laptop_check_end"
RUN+="/usr/bin/touch /run/udev/gdm-machine-is-laptop"
GOTO="gdm_hybrid_nvidia_laptop_check"
LABEL="gdm_laptop_check_end"

# If this is a hybrid graphics setup, take note
KERNEL!="card[1-9]*", GOTO="gdm_hybrid_graphics_check_end"
KERNEL=="card[1-9]-*", GOTO="gdm_hybrid_graphics_check_end"
SUBSYSTEM!="drm", GOTO="gdm_hybrid_graphics_check_end"
ACTION!="add", GOTO="gdm_hybrid_graphics_check_end"
RUN+="/usr/bin/touch /run/udev/gdm-machine-has-hybrid-graphics"
LABEL="gdm_hybrid_graphics_check_end"

# If this is a hybrid graphics laptop with vendor nvidia driver, disable wayland
########### gabmus commented this vvv
# LABEL="gdm_hybrid_nvidia_laptop_check"
# TEST!="/run/udev/gdm-machine-is-laptop", GOTO="gdm_hybrid_nvidia_laptop_check_end"
# TEST!="/run/udev/gdm-machine-has-hybrid-graphics", GOTO="gdm_hybrid_nvidia_laptop_check_end"
# TEST!="/run/udev/gdm-machine-has-vendor-nvidia-driver", GOTO="gdm_hybrid_nvidia_laptop_check_end"
# GOTO="gdm_disable_wayland"
# LABEL="gdm_hybrid_nvidia_laptop_check_end"

# Disable wayland in situation where we're in a guest with a virtual gpu and host passthrough gpu
LABEL="gdm_virt_passthrough_check"
TEST!="/run/udev/gdm-machine-has-hybrid-graphics", GOTO="gdm_virt_passthrough_check_end"
TEST!="/run/udev/gdm-machine-has-virtual-gpu", GOTO="gdm_virt_passthrough_check_end"
TEST!="/run/udev/gdm-machine-has-hardware-gpu", GOTO="gdm_virt_passthrough_check_end"
GOTO="gdm_disable_wayland"
LABEL="gdm_virt_passthrough_check_end"

# Disable wayland when there are multiple virtual gpus
LABEL="gdm_virt_multi_gpu_check"
TEST!="/run/udev/gdm-machine-has-hybrid-graphics", GOTO="gdm_virt_multi_gpu_check_end"
TEST!="/run/udev/gdm-machine-has-virtual-gpu", GOTO="gdm_virt_multi_gpu_check_end"
TEST=="/run/udev/gdm-machine-has-hardware-gpu", GOTO="gdm_virt_multi_gpu_check_end"
LABEL="gdm_virt_multi_gpu_check_end"

# Disable wayland when nvidia modeset is disabled or when drivers are a lower
# version than 470,
# For versions above 470 but lower than 510 prefer Xorg,
# Above 510, prefer Wayland.
KERNEL!="nvidia_drm", GOTO="gdm_nvidia_drm_end"
SUBSYSTEM!="module", GOTO="gdm_nvidia_drm_end"
ACTION!="add", GOTO="gdm_nvidia_drm_end"
# disable wayland if nvidia-drm modeset is not enabled
########### gabmus commented this vvv
# ATTR{parameters/modeset}!="Y", GOTO="gdm_disable_wayland"
# disable wayland for nvidia drivers versions lower than 470
########### gabmus commented this vvv
# ATTR{version}=="4[0-6][0-9].*|[0-3][0-9][0-9].*|[0-9][0-9].*|[0-9].*", GOTO="gdm_disable_wayland"
# For nvidia drivers versions Above 510, keep Wayland by default
ATTR{version}=="[5-9][1-9][0-9].*", GOTO="gdm_end"
# For nvidia drivers versions 470-495, prefer Xorg by default
GOTO="gdm_prefer_xorg"
LABEL="gdm_nvidia_drm_end"

GOTO="gdm_end"

LABEL="gdm_prefer_xorg"
RUN+="/usr/lib/gdm-runtime-config set daemon PreferredDisplayServer xorg"
GOTO="gdm_end"

LABEL="gdm_disable_wayland"
RUN+="/usr/lib/gdm-runtime-config set daemon WaylandEnable false"
GOTO="gdm_end"

LABEL="gdm_end"

```

# Multimedia

## Use pipewire-pulse pulseaudio replacement

```
sudo pacman -S pipewire pipewire-pulse pipewire-alsa
systemctl --user enable pipewire pipewire-pulse
```

Rebooting is a good idea.

### Noticeable audio delay on playback start

Edit `/etc/pipewire/media-session.d/media-session.conf`, comment line saying `suspend-node` inside an array called `default`.

Additionally edit `/etc/pipewire/media-session.d/alsa-monitor.conf`, uncomment the line saying `session.suspend-timeout-seconds = 5` and change its value to 0.

### Audio cutting out when multiple streams start playing

[Reference](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Troubleshooting#underrununderflow-and-broken-pipe-errors)

Edit `/etc/pipewire/media-session.d/alsa-monitor.conf`, uncomment the line saying `api.alsa.headroom = 0` and change its value to 1024.

## Fix bluetooth audio

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Bluetooth_headset)

Prevent GDM from spawning pulseaudio: edit `/var/lib/gdm/.config/pulse/client.conf` like so:

```
autospawn = no
daemon-binary = /bin/true
```

Finally run:

```bash
sudo -ugdm mkdir -p /var/lib/gdm/.config/systemd/user
sudo -ugdm ln -s /dev/null /var/lib/gdm/.config/systemd/user/pulseaudio.socket
```

## MPV hardware decoding (NVIDIA VDPAU)

Install `nvidia-utils` (or similar package depending on your nvidia driver) and `libva-vdpau-driver`.

Create or edit `.config/mpv/mpv.conf`:

```
vo=vdpau
profile=opengl-hq
hwdec=vdpau
hwdec-codecs=all
scale=ewa_lanczossharp
cscale=ewa_lanczossharp
interpolation
tscale=oversample
```

# Miscellaneous

## Sane touchpad scroll speed on Firefox and Geko based browsers

- Navigate to `about:config`
- Set the following properties to a lower value (10 is good for me)
    - `mousewheel.default.delta_multiplier_x`
    - `mousewheel.default.delta_multiplier_y`
    - `mousewheel.default.delta_multiplier_z`
- Set the `mousewheel.min_line_scroll_amount` property to something a lot higher (1000 is good for me)
  - This is to make sure scrolling with a mouse scroll wheel isn't unusably slow

## Enable Bluetooth support

`sudo systemctl enable --now bluetooth`

## Setup libvirt

```bash
sudo pacman -S libvirt ebtables dnsmasq bridge-utils virt-manager
sudo gpasswd -a $USERNAME libvirt
sudo gpasswd -a $USERNAME kvm
sudo systemctl enable libvirtd
sudo systemctl start libvirtd
```

Make sure to relogin after following the steps above. To create a network:

- Open virt-manager
- Click on *QEMU/KVM*
- Click *Edit > Connection Details* in the menu
- Click the *Virtual Networks* tab
- Click the `+` (plus sign) button in the bottom left corner of the newly opened window
- Name it whatever
- Select *NAT* as Mode
- Leave everything else as it is
- Click finish
- To start the network, select it in the sidebar and press the ▶️ (play icon) button
- To stop the network, press the icon to its left with the 🛑 (stop street sign icon) button (note: the icons could be different depending on the theme)
- To start the network on boot, select it in the sidebar and toggle the checkbox that says *Autostart: On Boot*

## Repository for Firefox Nightly

If you want to use the unstable, in-development version of Firefox known as Nightly, you can easily do so by adding the following repository to `/etc/pacman.conf`:

```
[heftig]
SigLevel = Optional
Server = https://pkgbuild.com/~heftig/repo/$arch
```
