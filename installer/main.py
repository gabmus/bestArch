#!/usr/bin/env python3

import os
import http.client as httplib
import disk
import re
import multiprocessing
import subprocess
# run_cmd = disk.run_cmd

EFIVARS_DIR = '/sys/firmware/efi/efivars'

EXIT_ERRORS = {
    'BIOS_MODE': 'Error: you\'re running in BIOS or CSM mode. This installer only supports UEFI.',
    'NO_CONNECTION': 'Error: no internet connection available. Try connecting to the internet with an ethernet cable.',
    'NO_WIFI': 'Error: no WiFi devices available or error with WiFi connection.',
    'SHELL': 'Error: Shell error. Something\'s wrong, please try again.',
    'NOT_IMPLEMENTED': 'Error: Path not implemented. You took the wrong decision.'
}

def failure_exit(errcode):
    print(EXIT_ERRORS[errcode])
    exit(1)

def yes_or_no(question):
    answer = ''
    while answer not in ['y', 'n']:
        answer = input(question).lower()
    return answer == 'y'

def have_internet():
    conn = httplib.HTTPConnection("archlinux.org", timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False

def run_cmd(cmd, errcode):
    try:
        subprocess.run(cmd, shell=True, check=True)
    except subprocess.CalledProcessError as err:
        failure_exit(errcode)

def prompt_wifi_connection():
    if yes_or_no('Do you want to try connecting over wifi? (y/n)\n>>> '):
        run_cmd('wifi-menu -o', 'NO_WIFI')
    else:
        failure_exit('NO_CONNECTION')


def tweak_pacman_conf(path):
    conf = None
    with open(path) as fd:
        conf = fd.readlines()
        fd.close()
    for i, line in enumerate(conf):
        if '#[multilib]' in line:
            conf[i] = conf[i][1:]
            conf[i+1] = conf[i+1][1:]
        if '#Color' in line or '#TotalDownload' in line:
            conf[i] = conf[i][1:]
    with open(path, 'w') as fd:
        fd.writelines(conf)
        fd.close()

def add_to_mkinitcpio_list(line, name, n_items, n_items_after=[], items_to_del=[]):
    pre = '{0}=('.format(name)
    suf = ')\n'
    line_l = line[len(pre):-len(suf)].split(' ')
    for i in n_items:
        line_l.append(i)
    for ia in n_items_after:
        for j, element in enumerate(line_l):
            if element == ia[0]:
                line_l.insert(j+1, ia[1])
                break
    for i, element in enumerate(line_l):
        if element in items_to_del:
            line_l.pop(i)
    final_line = '{0}{1}{2}'.format(
        pre,
        ' '.join(line_l),
        suf
    )
    return final_line


def tweak_mkinitcpio(path):
    conf = None
    with open(path) as fd:
        conf = fd.readlines()
        fd.close()

    for i, line in enumerate(conf):
        if re.match(r'^HOOKS=\((.*?)', line):
            conf[i] = add_to_mkinitcpio_list(
                line,
                'HOOKS',
                ['shutdown'],
                [
                    ('autodetect', 'keyboard keymap'),
                    ('block', 'encrypt lvm2')
                ]
            )
        elif re.match(r'^MODULES=\((.*?)', line):
            conf[i] = add_to_mkinitcpio_list(
                line,
                'MODULES',
                [
                    'lz4',
                    'lz4_compress'
                ]
            )
    lines_to_add = [
        '# GabMus\'s bestArch options\n'
        'COMPRESSION="lz4"\n',
        'COMPRESSION_OPTIONS="-9"\n'
    ]
    for n_line in lines_to_add:
        conf.append(n_line)

    with open(path, 'w') as fd:
        fd.writelines(conf)
        fd.close()

def tweak_makepkg(path):
    conf = None
    with open(path) as fd:
        conf = fd.readlines()
        fd.close()

    for i, line in enumerate(conf):
        if re.match(r'^COMPRESSXZ=\((.*?)', line):
            conf[i] = add_to_mkinitcpio_list(
                line,
                'COMPRESSXZ',
                ['--threads=0']
            )
        elif re.match(r'^COMPRESSGZ=\((.*?)', line):
            conf[i] = add_to_mkinitcpio_list(
                line,
                'COMPRESSGZ',
                [],
                [('gzip', 'pigz'),],
                ['gzip']
            )
    lines_to_add = [
        '# GabMus\'s bestArch options\n'
        'MAKEFLAGS="-j{0}"\n'.format(multiprocessing.cpu_count()-1),
    ]
    for n_line in lines_to_add:
        conf.append(n_line)

    with open(path, 'w') as fd:
        fd.writelines(conf)
        fd.close()


def generate_boot_entry(root_part_uuid, path, intel=False, amd=False, encrypted=False):
    conf = [
        'title        Arch Linux\n',
        'linux        /vmlinuz-linux\n',
        'initrd       /initramfs-linux.img\n'
    ]

    if intel:
        conf.insert(2, 'initrd       /intel-ucode.img\n')
    elif amd:
        conf.insert(2, 'initrd      /amd-ucode.img\n')

    options_last = ' rw quiet\n'
    options ='options      '
    
    if encrypted:
        options += 'cryptdevice=UUID={0}:cryptlvm root=/dev/MasterVolumeGroup/root'.format(root_part_uuid)
    else:
        options += 'root=UUID={0}'.format(root_part_uuid)

    options += options_last
    conf.append(options)

    with open(path, 'w+') as fd:
        fd.writelines(conf)
        fd.close()

def generate_bootloader_conf(path):
    with open(path, 'w+') as fd:
        fd.writelines([
            'default arch-*\n'
        ])
        fd.close()

def limit_journald(path):
    lines = None
    with open(path) as fd:
        lines = fd.readlines()
        fd.close()
    lines.append('SystemMaxUse=200M\n')
    with open(path, 'w') as fd:
        fd.writelines(lines)

def make_swap_file(path, size, fstab_path, relative_path): # relative_path refers to the path the file will have in the installed system
    print('Making swap file of size {0}...'.format(size))
    run_cmd('dd if=/dev/zero of={0} bs=1M count={1}'.format(path, size), 'SHELL')
    os.chmod(path, 0o600)
    run_cmd('mkswap {0}'.format(path), 'SHELL')
    fstab_lines = None
    with open(fstab_path) as fd:
        fstab_lines = fd.readlines()
        fd.close()
    fstab_lines.append('{0} none swap defaults 0 0\n'.format(path))
    with open(fstab_path, 'w') as fd:
        fd.writelines(fstab_lines)
        fd.close()

def set_timezone(zoneinfo_path, relative_zoneinfo_path, chroot_path):
    region = '-1'
    run = 0
    while not os.path.isdir('{0}/{1}'.format(zoneinfo_path, region)):
        if run > 0:
            print('Region name invalid, please select a valid region (Europe, America, Canada, Asia...)')
        if run > 1:
            print('Valid regions:')
            print(' '.join(os.listdir(zoneinfo_path)))
        run += 1
        region = input('Enter your region\n>>> ').strip()
        if region == '':
            region = '-1'
    city = '-1'
    run = 0
    while not os.path.isfile('{0}/{1}/{2}'.format(zoneinfo_path, region, city)):
        if run > 0:
            print('City name invalid, please select a valid city in the region you selected')
        if run > 1:
            print('Valid cities:')
            print(' '.join(os.listdir('{0}/{1}'.format(zoneinfo_path, region))))
        run += 1
        city = input('Enter your city\n>>> ').strip()
        if city == '':
            city = '-1'
    run_cmd(
        'arch-chroot {0} ln -sf {1}/{2}/{3} /etc/localtime'.format(
            chroot_path,
            relative_zoneinfo_path,
            region, city
        ),
        'SHELL'
    )
    run_cmd('arch-chroot {0} hwclock --systohc'.format(chroot_path), 'SHELL')

def generate_locale(chroot_path):
    conf = None
    with open('{0}/etc/locale.gen'.format(chroot_path)) as fd:
        conf = fd.readlines()
        fd.close()
    for i, line in enumerate(conf):
        if '#en_US.UTF-8 UTF-8' in line \
        or '#en_GB.UTF-8 UTF-8' in line \
        or '#it_IT.UTF-8 UTF-8' in line:
            conf[i] = conf[i][1:]
    with open('{0}/etc/locale.gen'.format(chroot_path), 'w') as fd:
        fd.writelines(conf)
        fd.close()
    run_cmd('arch-chroot {0} locale-gen'.format(chroot_path), 'SHELL')
    with open('{0}/etc/locale.conf'.format(chroot_path), 'w+') as fd:
        fd.writelines([
            'LANG=en_US.UTF-8\n'
        ])
        fd.close()

    keymap = disk.run_cmd('localectl --no-pager status | grep "VC Keymap"', 'SHELL', True).strip().split(' ')[2]
    with open('{0}/etc/vconsole.conf'.format(chroot_path), 'w+') as fd:
        fd.writelines([
            'KEYMAP={0}\n'.format(keymap)
        ])
        fd.close()


def select_pkg_groups():

    pkg_list_files_path = os.path.dirname(os.path.realpath(__file__))+'/../'

    pkg_list_files = [
        'commonpackages',
        'multilibpackages',
        'webdevpackages',
        'winepackages'
    ]

    if yes_or_no('Do you have an Intel CPU?\n>>> '):
        pkg_list_files.append('packages-intel')
    elif yes_or_no('Do you have an AMD CPU?\n>>> '):
        pkg_list_files.append('packages-amd')

    if yes_or_no('Do you have an Intel integrated GPU?\n>>> '):
        pkg_list_files.append('packages-intelgpu')
    if yes_or_no('Do you have an NVIDIA GPU (Maxwell or newer)?\n>>> '):
        pkg_list_files.append('packages-nvidiagpu')
        pkg_list_files.append('packages-nvidiagpu-32')
    if yes_or_no('Is this a laptop?\n>>> '):
        pkg_list_files.append('packages-laptop')

    mandatory = [
        'base',
        'base-devel'
    ]

    pkgs = ''
    
    for f in pkg_list_files:
        print('- {0}'.format(f))
    install_all = yes_or_no('Do you want to install all packages?\n>>> ')

    for f in pkg_list_files:
        f_path = pkg_list_files_path + f
        if install_all or yes_or_no('Do you want to install {0}?\n>>> '.format(f)):
            with open(f_path) as fd:
                pkgs += '{0} '.format(fd.read().strip())
                fd.close()
    
    for m in mandatory:
        if not m in pkgs:
            pkgs += '{0} '.format(m)

    return pkgs

def generate_hostname(chroot_path):
    hostname = ''
    while hostname == '':
        hostname = input('Choose a hostname (a name for your machine)\n>>> ').lower().strip()
        if ' ' in hostname:
            hostname=''
            print('Illegal characters detected')
        # TODO check if hostname contains illegal characters
    with open('{0}/etc/hostname'.format(chroot_path), 'w+') as fd:
        fd.writelines([
            '{0}\n'.format(hostname)
        ])
        fd.close()
    with open('{0}/etc/hosts'.format(chroot_path), 'w+') as fd:
        fd.writelines([
            '127.0.0.1        localhost\n',
            '::1              localhost\n',
            '127.0.1.1        {0}.localdomain    {0}\n'.format(hostname)
        ])
        fd.close()

def unlock_wheel(sudoers):
    conf = None
    with open(sudoers) as fd:
        conf = fd.readlines()
        fd.close()
    
    for i, line in enumerate(conf):
        if '# %wheel ALL=(ALL) ALL' in line:
            conf[i] = conf[i][2:]
            break
    
    with open(sudoers, 'w') as fd:
        fd.writelines(conf)
        fd.close()

def create_user(chroot_path):
    unlock_wheel('{0}/etc/sudoers'.format(chroot_path))
    username = ''
    while username == '':
        username = input('Choose a username\n>>> ').lower().strip()
        if ' ' in username:
            username = ''
            print('Illegal characters detected')
    name = ''
    while name == '':
        name = input('Input your full name\n>>> ').strip()
    groups = [
        'wheel',
        'sys',
        'lp',
        'cups'
    ]
    run_cmd(
        'arch-chroot {0} useradd -c "{1}" -G {2} -m {3}'.format(
            chroot_path, name, ','.join(groups), username
        ),
        'SHELL'
    )
    run_cmd('arch-chroot {0} passwd {1}'.format(chroot_path, username), 'SHELL')
    run_cmd('arch-chroot {0} passwd root'.format(chroot_path), 'SHELL')
    install_aur_packages(chroot_path, username)

def enable_systemd_services(chroot_path):
    services = [
        'gdm',
        'NetworkManager',
        'cups-browsed',
        'org.cups.cupsd'
    ]

    for s in services:
        run_cmd('arch-chroot {0} systemctl enable {1}'.format(chroot_path, s), 'SHELL')

def mkinitcpio(chroot_path):
    run_cmd('arch-chroot {0} mkinitcpio -p linux'.format(chroot_path), 'SHELL')

def install_aur_packages(chroot_path, username):
    if yes_or_no('Do you want to install the AUR packages?\n>>> '):
        aur_pkgs = None
        f_path = os.path.dirname(os.path.realpath(__file__))+'/../aurpackages'
        with open(f_path) as fd:
            aur_pkgs = fd.read().strip()
            fd.close()
        run_cmd(
            'arch-chroot {0} sudo -u {1} bash -c "cd; mkdir -p git/AUR; cd git/AUR; git clone https://aur.archlinux.org/yay.git; cd yay; makepkg -si; yay -S --noconfirm {2}"'.format(
                chroot_path, username, aur_pkgs
            ),
            'SHELL'
        )

# Check if running in UEFI mode
if not os.path.isdir(EFIVARS_DIR):
    failure_exit('BIOS_MODE')

if len(os.listdir(EFIVARS_DIR)) <= 0:
    failure_exit('BIOS_MODE')

# Check if connected to the internet
while not have_internet():
    print('No internet connection available.')
    prompt_wifi_connection()

print('Updating system clock...')
run_cmd('timedatectl set-ntp true', 'SHELL')

all_disks = disk.find_partitions()

print('Disks in this system:')
for d in all_disks:
    print(d)

print('''Do you want to use the recommended defaults?
    If you choose to do so, you will need to choose a disk.
    That disk will be erased, auto-partitioned and used for
    the installation.''')

if yes_or_no('Do you want to use the recommended defaults?\n>>> '):
    target_disk = disk.choose_disk(disk.find_partitions())
    target_parts = disk.partition_disk(target_disk)
    disk.mount_target_parts(target_parts)
    tweak_pacman_conf('/etc/pacman.conf')
    pkgs_list = select_pkg_groups()

    intel_ucode = 'intel-ucode' in pkgs_list
    amd_ucode = 'amd-ucode' in pkgs_list

    pacstrap_cmd = 'pacstrap /mnt {0}'.format(pkgs_list)
    run_cmd('pacman -Syy', 'SHELL')
    run_cmd(pacstrap_cmd, 'SHELL')
    tweak_pacman_conf('/mnt/etc/pacman.conf')
    run_cmd('genfstab -U /mnt >> /mnt/etc/fstab', 'SHELL')
    tweak_mkinitcpio('/mnt/etc/mkinitcpio.conf')
    tweak_makepkg('/mnt/etc/makepkg.conf')
    run_cmd('arch-chroot /mnt bootctl --path=/boot install', 'SHELL')

    root_uuid = ''
    all_disks = disk.find_partitions()
    for d in all_disks:
        if d.name == target_disk['diskname']:
            for p in d.partitions:
                if p.size > 600000000:
                    root_uuid = p.uuid
                    break
            break

    generate_boot_entry(root_uuid, '/mnt/boot/loader/entries/arch-linux.conf', intel=intel_ucode, amd=amd_ucode, encrypted=True)
    generate_bootloader_conf('/mnt/boot/loader/loader.conf')
    limit_journald('/mnt/etc/systemd/journald.conf')
    ram_size_mib = int(((os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES'))/1024**2)+1)
    make_swap_file('/mnt/home/swapfile', ram_size_mib, '/mnt/etc/fstab', '/home/swapfile')
    set_timezone('/mnt/usr/share/zoneinfo/', '/usr/share/zoneinfo/', '/mnt')
    generate_locale('/mnt/')
    generate_hostname('/mnt/')
    create_user('/mnt/')

    enable_systemd_services('/mnt/')

    # this could be ran last
    mkinitcpio('/mnt/')
    
else:
    failure_exit('NOT_IMPLEMENTED')
