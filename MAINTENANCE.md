# Arch Linux maintenance guide

## Regarding `pacman` and *AUR helpers*

On Arch Linux you're likely installing packages from the [Arch User Repository (AUR)](https://wiki.archlinux.org/title/Arch_User_Repository).

Typically it's a good idea to keep the number of AUR packages to a minimum for various reasons that I will not detail here.

Regardless, if you're using AUR packages, you're likely using a program called an **AUR helper**.

Similarly to your package manager (`pacman`) an AUR helper is a package manager that is able to manage (install, remove, update...) **both official and AUR packages**.

If you are using an AUR helper it's a good idea to always prefer using it over plain `pacman`.

If you're an experienced user, and you know what you're doing, you can call your shots on which one you prefer to use at any point. But if you're not as experienced, or if you're not sure which one to pick, it's a good idea to always opt for the AUR helper as, again, it can also do the same things pacman can.

The two most popular AUR helpers at the time of writing this (2021-08-28) are [`paru`](https://aur.archlinux.org/packages/paru-bin) and [`yay`](https://aur.archlinux.org/packages/yay-bin). My personal preference these days is `paru`, but both behave similarly.

One important thing to keep in mind is that **AUR helpers must not be used as root**, meaning you don't have to type `sudo paru` but just `paru`. This is counter-intuitive but it makes sense: AUR packages are built on your system in something called a *fakeroot environment*, and you can't have one if you're root. Don't be afraid, tho: most AUR helpers check if you're trying to run them as root, and will refuse to work, so you shouldn't be able to do any damage.

In the following examples I'll be assuming that you are using `paru` as your AUR helper, but you can drop in any other one you like instead.

### A quirk regarding `paru`

When using `paru` you might notice that at certain points it will pause and show you some files. Those files are called [PKGBUILDs](https://wiki.archlinux.org/title/PKGBUILD), they contain the instructions needed to create an AUR package, and it's a good practice to always give them a read before installing an AUR package. That's why `paru` shows them by default, and without asking you if you actually want to.

This can be daunting to new users, particularly if you don't know what the content of these files means. If you are certain that the package you're installing comes from a trusted source, then feel free to **just press `q`** on your keyboard to stop inspecting these files. If you can, try learning how to parse them, and how to make sense of them, after all it's for your security.

## Updating

Keeping your Arch Linux install up to date is of utmost importance. Running updates too far apart from one another can lead to several problems.

**Full system upgrade**: `sudo pacman -Syu` —or— `paru -Syu`

If you need to install a new package, and you haven't been updating in a while (~1 week), it's a good idea to update your system while installing the new package.

**Installing a new package**: `sudo pacman -S PACKAGENAME` —or— `paru -S PACKAGENAME`  
**Installing a new package while upgrading your system**: `sudo pacman -Syu PACKAGENAME` —or— `paru -Syu PACKAGENAME`

## Removing packages

Removing packages theoretically only requires the `-R` option, but there are more options you can use along with it to also remove other unwanted stuff.

- `n`: *nosave*, removes system configuration files relative to the package you're removing, even if you may have edited them. I always recommend using this option, unless you're removing a package that you know you'll need in the future and you want to retain your custom configuration.
- `c`: *cascade*, removes the package along with any other package that depends on it. This must be used with care, and it's always important to **review the list of packages to remove before proceeding**. Use this option without care and you are likely to break your install by removing useful or important packages like the Xorg server or your desktop environment.
- `s`: *recursive*, removes the package along with other packages that were installed as its dependencies. Very useful to keep your system clean and free of unwanted or unneeded programs and libraries. This can also be potentially dangerous, for the same reasons of the `c` option. Again, review the list of packages to remove every time you're removing a package.

This said, when removing packages I run:

`sudo pacman -Rsnc PACKAGENAME` —or— `paru -Rsnc PACKAGENAME`

And carefully check the list of packages to remove before proceeding.

If you try to remove a package, but you notice that it's trying to bring along with it some other packages that you need, try removing the `s` option first, then if that doesn't work the `c` option. If you get an error, you likely need that package after all.

## Cleaning your package cache

When installing packages, either from the official repos or from the AUR, those packages are downloaded and saved on your disk. This makes it easy to restore a package to a previous version if something goes wrong during an update, but since there is no built-in logic to clean that cache, **you'll likely end up with lots of wasted disk space** after some time.

To free up this space you can run:

- `sudo pacman -Sc` to remove old versions of cached official packages.
- `sudo pacman -Scc` to remove old and current cached official packages (note: don't worry, these commands won't uninstall anything, it just removes the cached installation files from your system)

Similarly you can run `paru -Sc` or `paru -Scc` to also remove cached AUR packages.

## Other useful commands

### Searching the repos for a package

`pacman -Ss SEARCH KEYWORDS` —or— `paru -Ss SEARCH KEYWORDS`

### Listing all of the files installed by a certain package

`pacman -Ql PACKAGENAME`

### Listing all packages installed in your system

`pacman -Q`

You can easily search for installed packages containing certain keywords by piping the result into grep, like this:

`pacman -Q | grep -i SEARCH KEYWORDS`

### Checking which package installed a certain file in your system

`pacman -Qo /SOME/FILE/PATH`

## Updating configuration files

In Linux, many programs rely on special configuration files to store settings and preferences. Configuration files can either be relative to your particular user account, or system wide. Many core packages, like Network Manager, Xorg or pacman itself use system wide configuration files.

Typically system wide configuration files can be found inside the `/etc` directory. It's possible that you may edit these configuration files by yourself, if you want to customize your system, enable a particular feature, or tweak the way some core programs work. If you followed the README in this repo you are very likely to have done so.

Once in a while when upgrading a package, pacman will try to overwrite a configuration file relative to it. But pacman is smart, it won't just overwrite a configuration file you have made changes to. If it detects that your particular configuration file isn't in its default state, it won't overwrite it. Instead it will place the new configuration file close to the original, adding a `.pacnew` extension to it.

For example, if the configuration file `/etc/someprogram.conf` is to be updated, the updated file will be placed in `/etc/someprogram.conf.pacnew`. This means that if there are any changes that you need to make, you will need to compare the two files yourself, and migrate the appropriate changes accordingly.

To make this process a little bit more automated, you can use an interactive program called `pacdiff` which is part of the `pacman-contrib` package.

To use `pacdiff` just run `sudo pacdiff`: it will interactively tell you which configuration files have a `.pacnew` associated with them, and then ask you to either use the `.pacnew`, the old file or to view the difference, so that you can make the changes you need to make by yourself.

## Flatpak

Flatpak is a universal, cross-distro, sandboxing package manager. You can use Flatpak in basically any modern Linux distro.

The main advantages of Flatpak over a regular package manager are:

- Packages are sandboxed. This means that a Flatpak application doesn't have access to your entire system, as other applications typically do. Flatpak has a granular permissions system that allows or denies certain applications to read or write certain locations, or if they can access certain system features or devices. This is particularly useful for untrustworthy and proprietary applications, such as the ever-so-popular Teams and Zoom.
- Compatibility is practically a guarantee. Flatpak packages have their dependencies bundled alongside the main application, this means that a developer can bundle the specific versions of the libraries that they target, without having to worry about version mismatches or downstream patches.

As a good rule of thumb, if a package isn't available in the main repos, or if you don't trust it fully, check if it's available as a Flatpak, then as a last resort you can try to install it from the AUR.

To install Flatpak, you just need to run `sudo pacman -S flatpak`, then use the `flatpak` command to install, remove and update Flatpak packages.

### System or user install?

Flatpak allows you to install packages either system-wide or for your own user. I typically recommend installing Flatpaks for your user only, to keep your system file structure cleaner.

To run Flatpak operations for your current user only, instead of running `flatpak`, just run `flatpak --user`.

### Flathub

Flatpak has many app repositories, the most popular and best curated one is [Flathub](https://flathub.org). To add the Flathub repository run `flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`.

### Install packages

`flatpak --user install PACKAGENAME` —or— `flatpak --user install APPLICATION_ID`

Where application ids are typically in the form of `com.author.appname`. Typically just the app name should be enough.

### Remove packages

`flatpak --user uninstall PACKAGENAME` —or— `flatpak --user uninstall APPLICATION_ID`

### Updating

`flatpak --user update`

### Searching for packages

`flatpak --user search KEYWORDS`
